# Changelog

All notable changes to this job will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

[![Pipeline](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/badges/trunk/pipeline.svg)](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/-/pipelines?ref=trunk)

## [0.4.1] - 2024-07-16

[![Pipeline](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/badges/0.4.1/pipeline.svg)](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/-/pipelines?ref=0.4.1)

* Update CICD

## [0.4.0] - 2024-07-16

[![Pipeline](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/badges/0.4.0/pipeline.svg)](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/-/pipelines?ref=0.4.0)

### Changed

* Job now always create a `version.txt` with the last version released on pypi if
  no version is missing in the registry

## [0.3.0] - 2024-07-16

[![Pipeline](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/badges/0.3.0/pipeline.svg)](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/-/pipelines?ref=0.3.0)

### Changed

* Job image tag is now fixed, regular updates will be done by hands

## [0.2.1] - 2024-07-13

[![Pipeline](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/badges/0.2.1/pipeline.svg)](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/-/pipelines?ref=0.2.1)

### Fixed

* Job now exit 1 when there is no version to release

## [0.2.0] - 2024-06-26

[![Pipeline](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/badges/0.2.0/pipeline.svg)](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/-/pipelines?ref=0.2.0)

* Update README.md

### Changed

* Renamed the job from `pypi_to_docker_missing_version` to `pypi-to-docker-missing-version`

## [0.1.1] - 2024-06-26

[![Pipeline](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/badges/0.1.1/pipeline.svg)](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/-/pipelines?ref=0.1.1)

* Documentation update
* Update continuous deployment pipeline
* Fix continuous integration pipeline stage

## [0.1.0] - 2024-06-26

[![Pipeline](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/badges/0.1.0/pipeline.svg)](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/-/pipelines?ref=0.1.0)

* Initial version
