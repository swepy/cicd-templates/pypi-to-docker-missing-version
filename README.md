# PyPI to Docker missing version

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/-/blob/main/LICENSE)
[![Pipeline](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/badges/0.4.1/pipeline.svg)](https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/-/pipelines?ref=0.4.1)

## Objective

Used to retrieve all versions of a package from PyPI and compare it to the version of
the image in the registry. If the version of the image is missing, the job will create
an artefact with the lowest version of the package missing in the registry, ready to be
used for other jobs to build the image.

## How to use it

### Include the component/template

Add the following to your `.gitlab-ci.yml` file.

#### As a Local component

Recommended if the component is local to the instance:

```yaml
include:
    -   component: $CI_SERVER_FQDN/swepy/cicd-templates/pypi-to-docker-missing-version/pypi-to-docker-missing-version@0.4.1
```

If your GitLab instance version is lower than `16.10`, use `$CI_SERVER_HOST` instead
of `$CI_SERVER_FQDN`. Don't forget to add the port if necessary.

#### As a Remote template

Recommended if the template is not local to the instance:

##### Source: lab.frogg.it

```yaml
include:
    -   remote: 'https://lab.frogg.it/swepy/cicd-templates/pypi-to-docker-missing-version/-/raw/0.4.1/templates/pypi-to-docker-missing-version.yml'
```

##### Mirror: GitLab.com

```yaml
include:
    -   remote: 'https://gitlab.com/swepy/cicd-templates/pypi-to-docker-missing-version/-/raw/0.4.1/templates/pypi-to-docker-missing-version.yml'
```

#### As a Local template

If the template is local to the instance:

```yaml
include:
    -   project: 'swepy/cicd-templates/pypi-to-docker-missing-version'
        ref: '0.4.1'
        file: 'templates/pypi-to-docker-missing-version.yml'
```

### Customize job

You can customize the job by overriding specific keys. For example:

```yaml
pypi_to_docker_missing_version:
    variables:
        SORT_CMD: grep -E "^[0-9]+(\.[0-9]+)*$" | sort -t. -k1,1n -k2,2n -k3,3n
```

## Variables

You can customize the job by overriding the following variables:

| Name                    | Description                                                                            | Default                                           |
|-------------------------|----------------------------------------------------------------------------------------|---------------------------------------------------|
| `IMAGE_NAME`            | **Mandatory** The name of the image. Should be "<org\|user>/\<repo>".                  | `""`                                              |
| `PACKAGE_INDEX_API_URL` | The URL of the API to retrieve the package index.                                      | `https://pypi.org/pypi`                           |
| `PACKAGE_NAME`          | The name of the package.                                                               | `"$CI_PROJECT_NAME"`                              |
| `REPOSITORIES_API_URL`  | The URL of the API to retrieve the repositories.                                       | `https://registry.hub.docker.com/v2/repositories` |
| `SORT_CMD`              | The command to sort the versions.                                                      | `sort -V`                                         |
| `TAGS`                  | The tags of the image to consider as already built. Used to exclude specific versions. | `""`                                              |
